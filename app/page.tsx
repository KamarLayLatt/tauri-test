"use client";

import { BookType, utils, write, read } from 'xlsx';
import { useState } from 'react';
import { save } from '@tauri-apps/api/dialog';
import { writeBinaryFile } from '@tauri-apps/api/fs';

export default function Home() {
  const [importedData, setImportedData] = useState(null);

  const handleImport = async (event: any) => {
    const file = event.target.files[0];
    const reader = new FileReader();

    const data = await file.arrayBuffer();
    const wb = read(data);

    /* generate array of presidents from the first worksheet */
    const ws = wb.Sheets[wb.SheetNames[0]]; // get the first worksheet
    const json: any = utils.sheet_to_json(ws); // generate objects

    /* update state */
    setImportedData(json); //
    handleExport(json);
  };


  const handleExport = async (employees: any | null) => {
    const data = [
      {
        id: '1',
        name: 'Saitama',
        age: '32'
      },
      {
        id: '2',
        name: 'Genos',
        age: "24"
      }
    ]

    const ws = utils.json_to_sheet(employees ?? data);
    const wb = utils.book_new();
    utils.book_append_sheet(wb, ws, "Data");
    await saveFile(wb);
  }

  const filters = [
    { name: "Excel Binary Workbook", extensions: ["xlsb"] },
    { name: "Excel Workbook", extensions: ["xlsx"] },
    { name: "Excel 97-2004 Workbook", extensions: ["xls"] },
    // ... other desired formats ...
  ];

  async function saveFile(wb: any) {
    /* show save file dialog */
    const selected = await save({
      title: "Save to Spreadsheet"
    });
    if (!selected) return;

    /* Generate workbook */
    // const fileExtension: BookType = selected.slice(selected.lastIndexOf(".") + 1);
    const d = write(wb, { type: "buffer", bookType: "xlsx" });

    /* save data to file */
    await writeBinaryFile(selected, d);
  }

  return <>
    <div className="flex justify-center mt-24">
      <button onClick={() => handleExport(null)} className="bg-gray-700 text-white p-4 rounded-lg">Export</button>
    </div>

    <div className="flex justify-center mt-24">
      <input type="file" accept=".xls, .xlsx, .xlsb" onChange={(e) => handleImport(e)} className="hidden" id="fileInput" />
      <label htmlFor="fileInput" className="bg-blue-500 text-white p-4 rounded-lg cursor-pointer">Import</label>
    </div>
    {importedData && (
      <div>
        <h2>Imported Data:</h2>
        <pre>{JSON.stringify(importedData, null, 2)}</pre>
      </div>
    )}
  </>
}